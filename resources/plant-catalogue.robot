*** Settings ***
Library  XML

*** Variables ***
${XML_MENU_PATH} =  ./inputs/xml_files/plant-catalogue.xml
${CATALOGUE_COUNT} =  1
${EXPECTED_PLANTS_NUM} =  36
${EXPECTED_GREEK_VALERIAN_PRICE} =  $4.36

*** Keywords ***
Check root count in XML
    ${root_count} =  get element count  ${XML_MENU_PATH}
    should be equal as numbers  ${root_count}  ${CATALOGUE_COUNT}

Check root name
    ${root} =  parse xml  ${XML_MENU_PATH}
    should be equal  ${root.tag}  CATALOG

Verify plants count
    ${plants_count} =  get element count  ${XML_MENU_PATH}  PLANT
    should be equal as integers  ${plants_count}  ${EXPECTED_PLANTS_NUM}

Verify polish name
    Check if plant contains polish name
    Compare polish name

Check a given plant price
    ${plant_price} =  get element  ${XML_MENU_PATH}  PLANT[32]/PRICE
    should be equal as strings  ${plant_price.text}  ${EXPECTED_GREEK_VALERIAN_PRICE}

Count plants that like shade
    ${shade_count} =  evaluate  0
    @{plants} =  get elements  ${XML_MENU_PATH}  PLANT
    :FOR  ${plant}  IN  @{plants}
    \  ${light} =  get element text  ${plant}  LIGHT
    \  ${shade_count}=  run keyword if  '${light}'=='Shade'  Evaluate  ${shade_count}+1  ELSE  evaluate  '${shade_count}'
    log  ${shade_count}

Add a new plant
    ${xml} =  parse xml  ${XML_MENU_PATH}
    add element  ${XML}  <PLANT></PLANT>
    add element  ${XML}  <COMMON><ENGLISH>Rose</ENGLISH><POLISH>Róża</POLISH></COMMON>  xpath=PLANT[37]
    add element  ${XML}  <ZONE>3</ZONE>  xpath=PLANT[37]
    add element  ${XML}  <LIGHT>Mostly Shady</LIGHT>  xpath=PLANT[37]
    add element  ${XML}  <PRICE>$7.32</PRICE>  xpath=PLANT[37]
    add element  ${XML}  <AVAILABILITY>012934</AVAILABILITY>  xpath=PLANT[37]
    save xml  ${XML}  ${XML_MENU_PATH}

Verify new plant exists
    ${xml} =  parse xml  ${XML_MENU_PATH}
    ${new_el} =  get element  ${XML}  PLANT[37]
    elements should be equal  ${new_el}  <PLANT><COMMON><ENGLISH>Rose</ENGLISH><POLISH>Róża</POLISH></COMMON><ZONE>3</ZONE><LIGHT>Mostly Shady</LIGHT><PRICE>$7.32</PRICE><AVAILABILITY>012934</AVAILABILITY></PLANT>

Verify if last plant was deleted
    element should exist  ${XML_MENU_PATH}  xpath=PLANT[37]
    remove element  ${XML_MENU_PATH}  xpath=PLANT[37]
    element should not exist  ${XML_MENU_PATH}  xpath=PLANT[37]

*** Keywords ***
Check if plant contains polish name
    ${common_children} =  get child elements  ${XML_MENU_PATH}  PLANT[2]/COMMON
    length should be  ${common_children}  2


Compare polish name
    ${polish_plant_name} =  get element  ${XML_MENU_PATH}  PLANT[2]/COMMON/POLISH
    should be equal as strings  ${polish_plant_name.text}  Orlik
