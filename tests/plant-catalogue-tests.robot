*** Settings ***
Resource  ../resources/plant-catalogue.robot

# robot -d results/xml tests/plant-catalogue-tests.robot

*** Variables ***


*** Test Cases ***
Verify Root Count
    Check root count in XML

Verify Root Name
    Check root name

Verify Plants Count
    Verify plants count

Verify Polish Plant Name
    Verify polish name

Check plant price
    Check a given plant price

Count plants which need shade light
    Count plants that like shade

Add a plant
    Add a new plant
    Verify new plant exists

Delete last plant
    Verify if last plant was deleted
